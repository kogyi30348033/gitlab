# frozen_string_literal: true

# rubocop: disable Gitlab/NamespacedClass
class Organization < ApplicationRecord
end
# rubocop: enable Gitlab/NamespacedClass
